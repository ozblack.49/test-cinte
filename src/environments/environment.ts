// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase:{
    apiKey: "AIzaSyA0mzYI20cig7i3zd3AdBblfX9uvYWdAmg",
    authDomain: "db-test-cinte.firebaseapp.com",
    databaseURL: "https://db-test-cinte.firebaseio.com",
    projectId: "db-test-cinte",
    storageBucket: "",
    messagingSenderId: "896858710712",
    appId: "1:896858710712:web:79becefbaccc6fb0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
