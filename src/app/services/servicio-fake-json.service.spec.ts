import { TestBed } from '@angular/core/testing';

import { ServicioFakeJSONService } from './servicio-fake-json.service';

describe('ServicioFakeJSONService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServicioFakeJSONService = TestBed.get(ServicioFakeJSONService);
    expect(service).toBeTruthy();
  });
});
