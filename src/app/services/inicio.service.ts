import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { TitlesData } from '../models/titles.model';


@Injectable({
  providedIn: 'root'
})
export class InicioService {

  listadoTitles: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase
  ) { }

  getList() {
    return this.listadoTitles = this.firebase.list('titles');
  }

  insertTitle(key: string, title: string) {
    this.listadoTitles.push({
      key,
      title
    });
  }

  updateTitle(key: string, title: string) {
    this.listadoTitles.update(key, {
      key,
      title 
    });
  }

  deleteTitle(key: string) {
    this.listadoTitles.remove(key)
  }
}
