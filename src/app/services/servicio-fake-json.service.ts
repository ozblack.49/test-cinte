import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ServicioFakeJSONService {

  constructor(
    private http: HttpClient
  ) { }


  getJSON(): Promise<any> {
    const url_api = 'https://jsonplaceholder.typicode.com/todos'
    return this.http.get(url_api).toPromise().then().catch(error => console.log(error))
  }

}
