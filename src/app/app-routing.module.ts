import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BorrarComponent } from './components/borrar/borrar.component';
import { EdicionComponent } from './components/edicion/edicion.component';
import { RegistroComponent } from './components/registro/registro.component';
import { ListaComponent } from './components/lista/lista.component';
import { ConsultaComponent } from './components/consulta/consulta.component';


const rutas: Routes = [
  { path: '', component: ListaComponent },
  { path: 'DeleteItem', component: BorrarComponent },
  { path: 'EditItem', component: EdicionComponent },
  { path: 'CreateItem', component: RegistroComponent },
  { path: 'ViewList', component: ConsultaComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(rutas)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
