export interface TitlesData {
    key: string;
    title: string;
}
