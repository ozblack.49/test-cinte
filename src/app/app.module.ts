import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

//firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import { ListaComponent } from './components/lista/lista.component';
import { RegistroComponent } from './components/registro/registro.component';
import { EdicionComponent } from './components/edicion/edicion.component';
import { BorrarComponent } from './components/borrar/borrar.component';
import { InicioComponent } from './components/inicio/inicio.component';

//services
import { InicioService } from './services/inicio.service';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsultaComponent } from './components/consulta/consulta.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    ListaComponent,
    RegistroComponent,
    EdicionComponent,
    BorrarComponent,
    InicioComponent,
    ConsultaComponent,
  ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [InicioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
