import { Component, OnInit } from '@angular/core';
import { ServicioFakeJSONService } from 'src/app/services/servicio-fake-json.service';
import { JsonItems } from 'src/app/models/jsonItems.model';
import { ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit {
  lsy: any[];
  listaJSON: JsonItems[];

  constructor(
    private dataApi: ServicioFakeJSONService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.obtenerListaParametros();
    this.toastr.warning('Se ve el JSON en la consola del log :/')
  }



  obtenerListaParametros() {
    this.dataApi.getJSON().
      then(data => {
        this.listaJSON = data;
        console.log(data)
      })
  }




}
