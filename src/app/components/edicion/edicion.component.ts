import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { InicioService } from 'src/app/services/inicio.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-edicion',
  templateUrl: './edicion.component.html',
  styleUrls: ['./edicion.component.scss']
})
export class EdicionComponent implements OnInit {

  key: string;
  title: string;
  titleForm: FormGroup;


  constructor(
    private titleService: InicioService,
    public formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.titleService.getList();
    this.buildForm();
  }

  buildForm() {
    this.titleForm = this.formBuilder.group({
      key: ['', Validators.compose([Validators.required])],
      title: ['', Validators.compose([Validators.required])]
    });
  }

  onEdit() {
    
    this.titleService.updateTitle(
      this.titleForm.get(['key']).value, 
      (this.titleForm.get(['title']).value));
  }

}
