import { Component, OnInit } from '@angular/core';
import { InicioService } from 'src/app/services/inicio.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-borrar',
  templateUrl: './borrar.component.html',
  styleUrls: ['./borrar.component.scss']
})
export class BorrarComponent implements OnInit {

  key: string;
  title: string;
  titleForm: FormGroup;

  constructor(
    private titleService: InicioService,
    public formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }

  buildForm() {
    this.titleForm = this.formBuilder.group({
      key: ['', Validators.compose([Validators.required])],
      title: ['', Validators.compose([Validators.required])]
    });
  }


  ngOnInit() {
    this.buildForm();
  }

  onDelete() {
    if (confirm('Esta seguro de eliminar --Hay un pequeño bug que elimina toda la DB XD--')) {
      this.titleService.deleteTitle(
        this.titleForm.get(['key']).value);
      this.toastr.success('-_-', 'OK');
    }
    this.toastr.info('Casi, hay que mejorar', '');
  }



}
