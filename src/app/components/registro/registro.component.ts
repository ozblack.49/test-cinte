import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InicioService } from 'src/app/services/inicio.service';
import { ToastrService} from 'ngx-toastr'
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  key: string;
  title: string;
  titleForm: FormGroup;

  constructor(
    private titleService: InicioService,
    public formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.titleService.getList();
    this.buildForm();
  }

  buildForm() {
    this.titleForm = this.formBuilder.group({
      key: ['', Validators.compose([Validators.required])],
      title: ['', Validators.compose([Validators.required])]
    });
  }

  onSubmit() {
    this.titleService.insertTitle(
      this.titleForm.get(['key']).value,
      (this.titleForm.get(['title']).value));
      this.toastr.success('Exitoso', 'OK');
  }

}
