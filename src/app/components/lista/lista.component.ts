import { Component, OnInit } from '@angular/core';
import { InicioService } from 'src/app/services/inicio.service';
import { TitlesData } from 'src/app/models/titles.model';


@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  listaTitles: TitlesData[];

  constructor(private titleService: InicioService) { }

  ngOnInit() {
    this.titleService.getList().snapshotChanges()
    .subscribe(item => {
      this.listaTitles = [];
      item.forEach(element => {
        let x = element.payload.toJSON()
        x["$key"] = element.key
        this.listaTitles.push(x as TitlesData)
      })
    })
  }

}
